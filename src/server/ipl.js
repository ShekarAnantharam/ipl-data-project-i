function getMatchesPlayedPerYearByAllTeams(matches){
    const matchesPerYear={};
    if (!Array.isArray(matches)){
        throw new Error("invalid arguments");
    }
    else{
         
        for(let match of matches){
            let season=match.season;
            if(matchesPerYear.hasOwnProperty(season)){
                matchesPerYear[season]+=1;
            }
            else{
                 matchesPerYear[season]=1;
            }
        }
    }
    return matchesPerYear;

}


function getMatchesWonPerTeamPerYear(matches){

    const matchesPerTeam={};
    if (!Array.isArray(matches)){
        throw new Error("invalid arguments");
    }
    else{
        for(let match of matches){
            let season=match.season;
            if(matchesPerTeam[match.winner]){
                if( matchesPerTeam[match.winner][season]){
                    matchesPerTeam[match.winner][season]+=1
                 }
                else{
                    matchesPerTeam[match.winner][season]=1;
                }

            }
            else{
                matchesPerTeam[match.winner]={};
                matchesPerTeam[match.winner][season]=1;
            }
        }
    }
    return matchesPerTeam;
}

//Problem 3:
//Extra runs conceded by each team in 2016
function getExtraRunsConcededPerTeamIn2016(deliveries,matches){
    const extraRunsPerTeam={};
    if(!Array.isArray(deliveries)&&!Array.isArray(matches)){
        throw new Error("invalid arguments");
    }
    else{
        for(let match of matches){
            if (match.season==2016){
                //console.log(match.season + parseInt(match.match_id));
                for(let delivery of deliveries){
                    
                     if (match.id==delivery.match_id){//console.log(delivery);break;return
                     
                         if(extraRunsPerTeam[delivery.bowling_team]){
                              extraRunsPerTeam[delivery.bowling_team]+=parseInt(delivery.extra_runs);
                        }
                             else{//extraRunsPerTeam[delivery.bowling_team]=;
                                 
                                  extraRunsPerTeam[delivery.bowling_team]=parseInt(delivery.extra_runs);
                            }
                        }
                }
            }
        }

    }
    return extraRunsPerTeam;
}

function getTop10EconomicalBowlersIn2015(matches,deliveries){
    const bowlerTotal={};
   
    if(!Array.isArray(deliveries)&&!Array.isArray(matches)){
        throw new Error("invalid arguments");
    }
    else{
        for(let match of matches){
            if (match.season==2015){
                for(let delivery of deliveries){
                    if (match.id==delivery.match_id){
                        if(bowlerTotal[delivery.bowler]){
                            bowlerTotal[delivery.bowler].runs+=parseInt(delivery.total_runs);
                            bowlerTotal[delivery.bowler].balls+=1;
                            if(parseInt(delivery.noball_runs)!=0||parseInt(delivery.wide_runs)!=0){
                                bowlerTotal[delivery.bowler].balls-=1;
                            }
                        }
                        else{
                            bowlerTotal[delivery.bowler]={};
                            bowlerTotal[delivery.bowler].balls=1;
                            bowlerTotal[delivery.bowler].runs=parseInt(delivery.total_runs);
                            if(parseInt(delivery.noball_runs)!=0||parseInt(delivery.wide_runs)!=0){
                                bowlerTotal[delivery.bowler].balls-=1;
                            }
                        }
                    }
                }
            }
        }
    }
   
    const economy={}, economyArr=[],top10Bowlers={};
    for(let bowler in bowlerTotal){
        let overs=(bowlerTotal[bowler].balls)/6;
        let economyOfBowler=Number((bowlerTotal[bowler].runs/overs).toFixed(2));
        economy[bowler]=economyOfBowler;
        economyArr.push(economyOfBowler);
    }
   economyArr.sort(function(a, b){return a-b});
   let count=0;
   for(let value of economyArr){
       
       for(let bowler in economy){
           
           if(value==economy[bowler]){
            console.log(economy[bowler]); 
               top10Bowlers[bowler]=value;
               count+=1;
               if(count==10){ return top10Bowlers}
           }
       }
   }
  
    
    
}
module.exports.getMatchesPlayedPerYearByAllTeams=getMatchesPlayedPerYearByAllTeams;
module.exports.getMatchesWonPerTeamPerYear=getMatchesWonPerTeamPerYear;
module.exports.getExtraRunsConcededPerTeamIn2016=getExtraRunsConcededPerTeamIn2016;
module.exports.getTop10EconomicalBowlersIn2015=getTop10EconomicalBowlersIn2015;