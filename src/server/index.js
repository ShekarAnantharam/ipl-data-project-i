const csv = require("csvtojson");
const fs=require("fs");
const mathesFilePath=("..//data/matches.csv");
const deliveriesFilePath=("../data/deliveries.csv");
const mathcesPerYearFunction=require("./ipl");
const mathcesWonPerTeamPerYearFunction=require("./ipl");
const extrRunsFunction=require("./ipl");
const top10BowlersFunction=require("./ipl");
const mathcesPerYearJSON="../public/output/jsonMatchesPerYear.json";
const mathcesWonPerTeamPerYearJSON="../public/output/jsonMatchesWonPerTeam.json";
const extrRunsFunctionJSON="../public/output/extrRunsConcededPerTeamIn2016.json";
const top10BowlersJSON="../public/output/top10EconomyBowlersIn2015.json";

csv()
    .fromFile(mathesFilePath)
    .then( matches => {  let result=mathcesPerYearFunction.getMatchesPlayedPerYearByAllTeams(matches);
        fs.writeFile(mathcesPerYearJSON,JSON.stringify(result),function(err){
            console.log(err);
         })
         let result2=mathcesWonPerTeamPerYearFunction.getMatchesWonPerTeamPerYear(matches);
        fs.writeFile(mathcesWonPerTeamPerYearJSON,JSON.stringify(result2),function(err){
            console.log(err);
        })
        
        csv()
        .fromFile(deliveriesFilePath)
        .then( deliveries => { let result3=extrRunsFunction.getExtraRunsConcededPerTeamIn2016(deliveries,matches);
            fs.writeFile(extrRunsFunctionJSON,JSON.stringify(result3),function(err){
                console.log(err);
            })
            let result4=top10BowlersFunction.getTop10EconomicalBowlersIn2015(matches,deliveries);
            console.log(result4);
            fs.writeFile(top10BowlersJSON,JSON.stringify(result4),function(err){
                console.log(err);
            })
        } )
} )
   // console.log(result)
    
